//
//  WebServices.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/23/20.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON



class WebServices {
    
    func getLocationDetails(locationId: String, completion: @escaping (LocationDetails?) -> ()) {
        
//        let request = AF.request("http://api.openweathermap.org/data/2.5/weather?q=\(locationId)")
        let url = "https://5fbde9185923c90016e6a37f.mockapi.io/api/v18/details"
        
        AF.request(url, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value).arrayValue
                print("JSON: \(json)")
                //TODO - Refactor when integrate
                for locationDetail in json {
                    print(locationDetail)
                    let locDetail = LocationDetails(json: locationDetail)
                    completion(locDetail)
                }
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
        
    }
    
    func getDataForLocations(urlForService: String, completion: @escaping ([LocationData]?) -> ()) {
        //us - https://5fbde9185923c90016e6a37f.mockapi.io/api/v18/us
        //tx - https://5fbde9185923c90016e6a37f.mockapi.io/api/v18/counties
        
//        let request = AF.request(urlForService)
        
//        let url = "https://5fbde9185923c90016e6a37f.mockapi.io/api/v18/us"
        
        AF.request(urlForService, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                var locationsDataToReturn = [LocationData]()
                let json = JSON(value).arrayValue
                print("JSON: \(json)")
                for locationState in json {
                    print(locationState)
                    let location = LocationData(json: locationState)
                    locationsDataToReturn.append(location)
                }
                completion(locationsDataToReturn)
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }
    }
}
