//
//  LocationStats.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/23/20.
//

import Foundation
import SwiftyJSON

struct LocationData: Codable, Hashable {
    var name: String
    var id: String
    var newCases: Int
    var confirmedCases: Int
    var deathCases: Int
    var rT: Double
    
    init(name: String, id: String, newCases: Int, confirmedCases: Int, deathCases: Int, rT: Double){
        self.name = name
        self.id = id
        self.newCases = newCases
        self.confirmedCases = confirmedCases
        self.deathCases = deathCases
        self.rT = rT
    }
    
    init(json: JSON) {
        self.name = json["name"].stringValue
        self.id = json["id"].stringValue
        self.newCases = json["newCases"].intValue
        self.confirmedCases = json["confirmedCases"].intValue
        self.deathCases = json["deathCases"].intValue
        self.rT = json["rT"].doubleValue
    }
}

var TestLocation = LocationData(name:  "Alabama", id: "AL", newCases: 164545, confirmedCases: 199640, deathCases: 113144, rT: 1.3496)
