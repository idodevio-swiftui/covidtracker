//
//  StatCategory.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/17/20.
//

enum StatCategory: String {
    case Actives
    case Deaths
    case Confirmed
    case NewCases
}
