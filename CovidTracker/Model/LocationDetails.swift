//
//  CountyDetailsModel.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/23/20.
//

import Foundation
import SwiftyJSON

struct LocationDetails: Codable, Hashable {
    var name: String
    var newCases: [Double]
    var confirmedCases: [Double]
    var deathCases: [Double]
    var rT: [Double]
    
    init() {
        self.name = "n/a"
        self.newCases = []
        self.confirmedCases = []
        self.deathCases = []
        self.rT = []
    }
    
    init(name: String, newCases: [Double], confirmedCases: [Double], deathCases: [Double], rT: [Double]){
        self.name = name
        self.newCases = newCases
        self.confirmedCases = confirmedCases
        self.deathCases = deathCases
        self.rT = rT
    }
    
    init(json: JSON ) {
        self.name = json["name"].stringValue
        self.newCases = []
        self.confirmedCases = []
        self.deathCases = []
        self.rT = []
        
        if let newCases = json["newCases"].arrayObject as? [Double] {
            self.newCases = newCases
        }
        
        if let confirmedCases = json["confirmedCases"].arrayObject as? [Double] {
            self.confirmedCases = confirmedCases
        }
        
        if let deathCases = json["deathCases"].arrayObject as? [Double] {
            self.deathCases = deathCases
        }
        
        if let rT = json["rT"].arrayObject as? [Double] {
            self.rT = rT
        }
        
    }
}


let Test_Location_Detail = LocationDetails(name: "Dallas", newCases: [
    8041.00,
    8090,
    9909,
    34230,
    38442,
    40100,
    66755,
    75355,
    85932,
    87631,
    98060,
    99790,
    104749,
    105225,
    106890,
    113962,
    130177,
    168114,
    181224,
    185442,
    190268,
    214845,
    225046,
    235479,
    239896,
    257563,
    258817,
    261998,
    281933,
    297746
], confirmedCases: [8,23,54,32,12,37,7,23,43], deathCases: [8,23,54,32,12,37,7,23,43], rT: [8,23,54,32,12,37,7,23,43])
