//
//  ContentView.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/17/20.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var countryDataVM: CountryDataViewModel
    @State private var searchText : String = ""
    
    init() {
        self.countryDataVM = CountryDataViewModel()
    }
    
    var body: some View {
        NavigationView {
            VStack{
                SearchBar(text: $searchText, placeholder: "Search State")
                ScrollView {
                    ForEach(self.countryDataVM.listOfStatesAndData.filter {
                        self.searchText.isEmpty ? true : $0.name.lowercased().contains(self.searchText.lowercased())
                    }, id: \.self) { state in
                        NavigationLink(destination: StateDataView(selectedStateName: state.name)) {
                            DataCard(recordToPresent: state)
                        }
                    }
                }
                .padding(.horizontal, 10.0)
                .navigationBarTitle(Text("Covid 19 Tracker"))
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
