//
//  CovidTrackerApp.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/17/20.
//

import SwiftUI

@main
struct CovidTrackerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
