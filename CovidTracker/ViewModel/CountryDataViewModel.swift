//
//  CountryStatsViewModel.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/21/20.
//

import Foundation
import Combine



class CountryDataViewModel:ObservableObject {
    let API_URL = "https://5fbde9185923c90016e6a37f.mockapi.io/api/v18/us"
    
    private var webServices: WebServices!
    
    @Published var listOfStatesAndData = [LocationData]()
    
    init() {
        self.webServices = WebServices()
        fetchDataForCountry()
    }
    
    func fetchDataForCountry() {
        print("IDO - fetching states Data")
        self.webServices.getDataForLocations(urlForService: API_URL){ listOfStates in
            if let listOfStates = listOfStates {
                DispatchQueue.main.async {
                    self.listOfStatesAndData = listOfStates
                }
            }
        }
    }
}
