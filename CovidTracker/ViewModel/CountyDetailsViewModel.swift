////
////  CountyDetailsViewModel.swift
////  CovidTracker
////
////  Created by Ido Doron on 11/26/20.
////
//
//import Foundation
//import Combine
//
//
//
//class CountyDetailsViewModel:ObservableObject {
//    let API_URL = "https://5fbde9185923c90016e6a37f.mockapi.io/api/v18/details"
//
//    private var webServices: WebServices!
//    
//    @Published var selectedCountyDetails = LocationDetails()
//    
//    init(param: String) {
//        self.webServices = WebServices()
//        fetchDetailsForSelectedCounty(countyName: param)
//    }
//    
//    func fetchDetailsForSelectedCounty(countyName: String) {
//        self.webServices.getLocationDetails(locationId: countyName) { countyDetails in
//            if let countyDetails = countyDetails {
//                self.selectedCountyDetails = countyDetails
//            }
//        }
//    }
//    
//}
