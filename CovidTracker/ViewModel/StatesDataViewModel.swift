//
//  StatesStatsViewModel.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/25/20.
//

import Foundation
import Combine

class StatesDataViewModel:ObservableObject {
    
    let API_URL = "https://5fbde9185923c90016e6a37f.mockapi.io/api/v18/counties"
    private var webServices: WebServices!
    
    @Published var listOfCountiesAndData = [LocationData]()
    @Published var selectedCountyDetails = LocationDetails()
    
    
    init() {
        self.webServices = WebServices()
    }
    
    func fetchDataForState(selectedState: String) {
        print("IDO - fetching Data for \(selectedState)")
        self.webServices.getDataForLocations(urlForService: API_URL){ listOfCounties in
            if let listOfCounties = listOfCounties {
                DispatchQueue.main.async {
                    self.listOfCountiesAndData = listOfCounties
                }
            }
        }
    }
    
    func fetchDetailsForSelectedCounty(selectedCountyName: String) {
        print("IDO - fetching data for \(selectedCountyName) County")
        self.webServices.getLocationDetails(locationId: selectedCountyName) { countyDetails in
            if let countyDetails = countyDetails {
                DispatchQueue.main.async {
                    self.selectedCountyDetails = countyDetails
                }
            }
        }
    }
    
    
}
