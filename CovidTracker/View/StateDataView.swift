//
//  StateStats.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/21/20.
//

import SwiftUI

struct StateDataView: View {
    @Environment(\.presentationMode) var presentationMode
    
    var selectedStateName: String
    @State private var countySearch : String = ""
    @ObservedObject private var statesDataVM = StatesDataViewModel()
    
    var body: some View {
        VStack{
            SearchBar(text: $countySearch, placeholder: "Search County")
            ScrollView {
                ForEach(self.statesDataVM.listOfCountiesAndData.filter {
                    self.countySearch.isEmpty ? true : $0.name.lowercased().contains(self.countySearch.lowercased())
                }, id: \.self) { county in
                    NavigationLink(destination: CountyDetailsView(selectedCounty: self.statesDataVM.selectedCountyDetails)
                                    .onAppear{ self.statesDataVM.fetchDetailsForSelectedCounty(selectedCountyName: county.name) }
                    ) {
                        DataCard(recordToPresent: county)
                    }
                }
            }
            .onAppear{
                self.statesDataVM.fetchDataForState(selectedState: selectedStateName)
            }
            .padding(.horizontal, 10.0)
            .navigationBarTitle(selectedStateName,displayMode: .inline)
            .navigationBarBackButtonHidden(true)
            // Add your custom back button here
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        HStack {
                                            Image(systemName: "chevron.left    ")
                                            Text("Back")
                                        }
                                    })
        }
    }
    
    struct StateStats_Previews: PreviewProvider {
        static var previews: some View {
            StateDataView(selectedStateName: "Texas")
        }
    }
}
