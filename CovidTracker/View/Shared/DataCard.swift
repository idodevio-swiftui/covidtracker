//
//  StatCard.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/17/20.
//

import SwiftUI

struct DataCard: View {
    var recordToPresent: LocationData
    var body: some View {
        HStack {
            Text(recordToPresent.name)
            Spacer()
            DataItem(number: recordToPresent.newCases.formatUsingAbbrevation(), category: .NewCases, arrow:true)
            DataItem(number: recordToPresent.confirmedCases.formatUsingAbbrevation(), category: .Confirmed, arrow:true)
            DataItem(number: recordToPresent.deathCases.formatUsingAbbrevation(), category: .Deaths, arrow:false)
            
        }
        .padding(.all)
        .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/, width: 1.5)
        .cornerRadius(4)
        .background(Color.white)
        .clipped()
        .shadow(color: Color.gray, radius: 3, x: 0, y: 0)
    }
}

struct DataCard_Previews: PreviewProvider {
    static var previews: some View {
        DataCard(recordToPresent: TestLocation)
    }
}

struct DataItem: View {
    let number: String
    let category: StatCategory
    let arrow: Bool
    
    var body: some View {
        VStack {
            HStack {
                Image(systemName: "arrow.\(arrow ? "up" : "down")")
                Text("\(number)")
                    .font(.callout)
            }
            Text("\(category.rawValue)")
                .font(.footnote)
        }
    }
}
