//
//  CountyDetails.swift
//  CovidTracker
//
//  Created by Ido Doron on 11/21/20.
//

import SwiftUI
import SwiftUICharts

struct CountyDetailsView: View {
    var selectedCounty: LocationDetails
    @State private var timeFrame = 7
    
    var body: some View {
        VStack{
            Picker(selection: $timeFrame, label: Text("Time Frame?")) {
                Text("30 Days").tag(30)
                Text("14 Days").tag(14)
                Text("7 days").tag(7)
            }.padding(.vertical).pickerStyle(SegmentedPickerStyle())
//            ScrollView {
            BarChartView(data: ChartData(points: Array(selectedCounty.newCases.suffix(timeFrame))), title: "New", form: ChartForm.large)
                .padding(.vertical)
            BarChartView(data: ChartData(points: Array(selectedCounty.confirmedCases.suffix(timeFrame))), title: "Confirmed/Active", form: ChartForm.large)
                .padding(.vertical)
            BarChartView(data: ChartData(points: Array(selectedCounty.deathCases.suffix(timeFrame))), title: "Deaths", form: ChartForm.large)
                .padding(.vertical)
            BarChartView(data: ChartData(points:Array(selectedCounty.rT.suffix(timeFrame))), title: "rT", form: ChartForm.large)
                .padding(.vertical)
            Spacer()
            
        }
        .padding(.horizontal)
        .navigationBarTitle("\(selectedCounty.name) County")
        //        .padding(.horizontal, 10.0)
    }
}

struct CountyDetails_Previews: PreviewProvider {
    static var previews: some View {
        CountyDetailsView(selectedCounty: Test_Location_Detail)
    }
}
